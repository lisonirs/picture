package service;

import java.util.ArrayList;

import application.Controller;

import application.Image;
import application.ImagePersonne;
import application.Personne;

public class Modele {
	
	private static ArrayList<Personne> listePersonne = new ArrayList<Personne>();
	
	ArrayList<Image> listeImage = new ArrayList<Image>();
	
	ArrayList<ImagePersonne> listeNote = new ArrayList<ImagePersonne>();
	
	public Modele() {
		super();
		Personne p1 = new Personne("Lemercier", "jeanL");
		Personne p2 = new Personne("Vidal", "paulV");
		
		listePersonne.add(p1);
		listePersonne.add(p2);
		
		Image i1 = new Image("Paris", "paris.jpg");
		Image i2 = new Image("Marseille", "marseille.jpg");
		
		listeImage.add(i1);
		listeImage.add(i2);
		
		ImagePersonne ip1 = new ImagePersonne(p1, i1, 10);
		ImagePersonne ip2 = new ImagePersonne(p2, i2, 15);
		
		listeNote.add(ip1);
		listeNote.add(ip2);

	}

	public static ArrayList<Personne> getListePersonne() {
		return listePersonne;
	}

	public void setListePersonne(ArrayList<Personne> listePersonne) {
		this.listePersonne = listePersonne;
	}
	

	
	
}
