package application;

import service.Modele;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import service.Modele;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class Controller implements Initializable{
	
	@FXML
	private TextField nom;
	
	@FXML
	private TextField login;
	
	@FXML
	private TextField note;
	
	@FXML
	private Label error;
	
	@FXML
	private Label votreNom;
	
	@FXML
	private Label votreLogin;
	
	@FXML
	private Label notezlImage;
	
	@FXML
	private Button validerUser;
	
	@FXML
	private Button validerNote;
	
	@FXML 
	private ComboBox<String> villeCombo;

	@FXML 
	private ImageView ParisImage;

	@FXML 
	private ImageView MarseilleImage; 

	@FXML 
	private Label selectedVille;
	
	private Modele model;
	
	public void validerPersonne(ActionEvent e) {
		String nomPersonne = nom.getText();
		String loginPersonne = login.getText();
		
		if(Modele.getListePersonne().contains(nomPersonne) && Modele.getListePersonne().contains(loginPersonne)) {
			error.setText("Utilisateur trouv�");
		}else {
			error.setText("Utilisateur non trouv�");
			selectedVille.setVisible(false);
			nom.setVisible(false);
			login.setVisible(false);
			note.setVisible(false);
			villeCombo.setVisible(false);
			votreNom.setVisible(false);
			votreLogin.setVisible(false);
			notezlImage.setVisible(false);
			validerUser.setVisible(false);
			validerNote.setVisible(false);
			
		}
		
	}
	
	public void validerNote(ActionEvent e) {
		error.setText("");
		String noteIP = note.getText();
		String nomPersonne = nom.getText();
		String loginPersonne = login.getText();
		
		try {
			FileWriter resultatsNotes = new FileWriter("Notes.txt");
			resultatsNotes.write(nomPersonne + " ;" + loginPersonne + " ;" + noteIP);
			resultatsNotes.write("\n");
			
			resultatsNotes.close();
			
			}catch(FileNotFoundException f){
				error.setText("Fichier non trouv�");
				
			}catch(IOException f){
				error.setText("Erreur de fichier");

			}
	}
	
	public static List<String[]> readFile(String urlFichier) {
        BufferedReader lecteurAvecBuffer = null;
        List<String[]> listeDocument = new ArrayList<String[]>();
        try {
            lecteurAvecBuffer =
                new BufferedReader(
                    new InputStreamReader(new FileInputStream(urlFichier), "UTF8"));
            String ligne = "";
            int nbLigne = 1;
            while ((ligne = lecteurAvecBuffer.readLine()) != null) {
                if (nbLigne > 0) {
                    String[] tab = ligne.split(";");
                    if (tab.length > 1) {
                        listeDocument.add(tab);
                    }
                }
                nbLigne++;
            }
        } catch (IOException e) {
           System.out.println(e.getMessage());
        } finally {
            try {
                if (lecteurAvecBuffer != null) {
                    lecteurAvecBuffer.close();
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return listeDocument;
    }
	
	@Override
	  public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
		List<String[]> listing = this.readFile("Notes.txt");
		
		ParisImage.setVisible(false);
		MarseilleImage.setVisible(false);
		
	    villeCombo.getItems().setAll("Paris", "Marseille");

	    selectedVille.textProperty().bind(villeCombo.getSelectionModel().selectedItemProperty());

	    villeCombo.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
	     
	    	@Override 
	    	public void changed(ObservableValue<? extends String> selected, String oldVille, String newVille) {
	        if (oldVille != null) {
	          switch(oldVille) {
	            case "Paris": ParisImage.setVisible(false); 
	            break;
	            case "Marseille": MarseilleImage.setVisible(false); 
	            break;
	          }
	        }
	        if (newVille != null) {
	          switch(newVille) {
	            case "Paris": ParisImage.setVisible(true); 
	            break;
	            case "Marseille": MarseilleImage.setVisible(true); 
	            break;
	          }
	        }
	      }
	    });
	  }
}
