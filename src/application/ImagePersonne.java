package application;

public class ImagePersonne {
	private int note;
	
	private Personne personne;
	
	private Image image;
	
	
	public ImagePersonne(Personne personne, Image image, int note) {
		super();
		this.personne = personne;
		this.image = image;
		this.note = note;
		
	}


	public int getNote() {
		return note;
	}


	public void setNote(int note) {
		this.note = note;
	}


	public Personne getPersonne() {
		return personne;
	}


	public void setPersonne(Personne personne) {
		this.personne = personne;
	}


	public Image getImage() {
		return image;
	}


	public void setImage(Image image) {
		this.image = image;
	}
}
